<?php
$cnf['role'] = [
    '1' => 'Super Admin',
    '2' => 'User',
];
$cnf['is_verified'] = [
    '1' => 'Verified',
    '0' => 'Not Verified',
];

$cnf['status'] = [
    '1' => 'Active',
    '0' => 'Deactive',
];
$cnf['gender'] = [
    '1' => 'Male',
    '0' => 'Female',
];
return $cnf;
?>