<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- Font Awesome -->
  <!-- <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css"> -->
  {!! Html::style('public/css/all.min.css') !!}
  {!! Html::style('public/css/tempusdominus-bootstrap-4.min.css') !!}
  {!! Html::style('public/css/icheck-bootstrap.min.css') !!}
 
  {!! Html::style('public/css/adminlte.min.css') !!}
  {!! Html::style('public/css/OverlayScrollbars.min.css') !!}
  {!! Html::style('public/css/daterangepicker.css') !!}
  {!! Html::style('public/css/summernote-bs4.css') !!}
  
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition skin-blue sidebar-mini"style="padding: 0px">
<div class="wrapper">

            @include('element.header')
            @include('element.slider')
            @yield('content')
            @include('element.footer')
 
</div>
<!-- ./wrapper -->

<!-- jQuery -->

{!! Html::script('public/js/jquery.min.js')!!}
{!! Html::script('public/js/jquery-ui.min.js')!!}
{!! Html::script('public/js/bootstrap.bundle.min.js')!!}
{!! Html::script('public/js/Chart.min.js')!!}
{!! Html::script('public/js/sparkline.js')!!}
{!! Html::script('public/js/jquery.knob.min.js')!!}
{!! Html::script('public/js/moment.min.js')!!}
{!! Html::script('public/js/daterangepicker.js')!!}
{!! Html::script('public/js/tempusdominus-bootstrap-4.min.js')!!}
{!! Html::script('public/js/summernote-bs4.min.js')!!}
{!! Html::script('public/js/jquery.overlayScrollbars.min.js')!!}
{!! Html::script('public/js/adminlte.js')!!}
{!! Html::script('public/js/dashboard.js')!!}
{!! Html::script('public/js/demo.js')!!}

<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
</body>
</html>
