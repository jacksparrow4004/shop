@extends('layout.app')
@section('content')
<div class="content-wrapper">
   
    <!-- Main content -->
    <section class="content">

        <!-- COLOR PALETTE -->
        <div class="box box-default color-palette-box">
            <div class="box-header with-border">
                <h3 class="box-title">Admin Details</h3>
            </div>

            <form action="{{route('admin.store')}}" method="post" id="add_form"  class="center-block">
            {{csrf_field()}}
                <div class="box-body">
                    <div class="col-md-12">

                        <div class="row"> 
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>First Name <span class="error">*</span></label>
                                    <input type="text" class="form-control allCaps" name="first_name" data-bvalidator="required,alpha" placeholder="First Name">
                                </div>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Last Name <span class="error">*</span></label>
                                    <input type="text" class="form-control allCaps" name="last_name" data-bvalidator="required,alpha" placeholder="Last Name">
                                </div>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Email Address <span class="error">*</span></label>
                                    <input type="email" class="form-control" autocomplete="off" name="email" data-bvalidator="required" placeholder="Email Address">
                                </div>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Password <span class="error">*</span></label>
                                    <input type="password" class="form-control" autocomplete="off" id="pass" name="password" data-bvalidator="required" placeholder="Password">
                                </div>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Confirm Password <span class="error">*</span></label>
                                    <input type="password" class="form-control" name="c_password" autocomplete="off" data-bvalidator="required,equal[pass]" placeholder="Confirm Password">
                                </div>
                            </div>
                        </div>
                        <div class="row"> 
                           <div class="col-sm-6">                                   
                                <div class="form-group">
                                    <label  class="form-group">Mobile No<span class="requird">*</span></label>   
                                    <input type="mobile" class="form-control onlyNumbers" maxlength="10" minlength="10" name="mobile_no" data-bvalidator="required" placeholder="Mobile">
                                </div>                              
                            </div> 
                        </div>

                    </div>
                </div>
                <!-- /.box-body -->

                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="step1">
                        <div class="box-footer">    
                            <div class="col-md-6"> 

                                <button type="submit" class="btn btn-primary">Save</button>
                                <a class="btn btn-primary prev-step" href="{{route('admin.index')}}" type="submit">Cancel</a>
                            </div> 
                        </div>
                    </div>
                </div>   
            </form>
        </div>
        <!-- /.box -->

        <!-- /.box-body -->


    </section>
    <!-- /.content -->

</div>
<!--<script type="text/javascript">
    $(document).ready(function () {
        $('#add_form').bValidator();
    });
</script>-->
@endsection



