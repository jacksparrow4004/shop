@extends('layout.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
          @include('element.flash')
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- COLOR PALETTE -->
        <div class="box box-default color-palette-box">
            <div class="box-header with-border">
                <h3 class="box-title">Admin Edit</h3>
            </div>

            <form action="{{route('admin.update',$data)}}" method="post" id="edit_admin"  class="center-block">
                {{csrf_field()}}
                {{ method_field('PATCH')}}
                <div class="box-body">
                    <div class="col-md-12">

                        <div class="row"> 
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>First Name <span class="error">*</span></label>
                                    <input type="text" value="{{ old('first_name',$data->first_name) }}" class="form-control allCaps" name="first_name" data-bvalidator="required,alpha" placeholder="First Name">
                                </div>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Last Name <span class="error">*</span></label>
                                    <input type="text" value="{{ old('last_name',$data->last_name) }}" class="form-control allCaps" name="last_name" data-bvalidator="required,alpha" placeholder="Last Name">
                                </div>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Email<span class="error">*</span></label>
                                    <input type="email" value="{{ old('email',$data->email) }}" class="form-control" name="email" data-bvalidator="required" placeholder="Email">
                                </div>
                            </div>
                        </div>
                        <div class="row"> 
                           <div class="col-sm-6">                                   
                                <div class="form-group">
                                    <label  class="form-group">Mobile No<span class="requird">*</span></label>   
                                    <input type="mobile" class="form-control onlyNumbers" value="{{ old('mobile_no',$data->mobile_no) }}" maxlength="10" minlength="10" name="mobile_no" data-bvalidator="required,digit" placeholder="Mobile">
                                </div>                              
                            </div> 
                        </div>

                    </div>
                </div>
                <!-- /.box-body -->

                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="step1">
                        <div class="box-footer">    
                            <div class="col-md-6"> 

                                <button type="submit" class="btn btn-primary">Save</button>
                                <a class="btn btn-primary prev-step" href="{{route('admin.index')}}" type="submit">Cancel</a>
                            </div> 
                        </div>
                    </div>
                </div>   
            </form>
        </div>
        <!-- /.box -->

        <!-- /.box-body -->


    </section>
    <!-- /.content -->



</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#edit_admin').bValidator();

    });
</script>
@endsection
