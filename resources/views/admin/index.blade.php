@extends('layout.app')
@section('content')
<div class="content-wrapper">
    
    <section class="content">
        <div class="box box-default ">
            <div class="box-header with-border">
                <h3 class="box-title">Admin Users</h3>
                <div class="pull-right">
                        <a href="{{route('admin.create')}}" class="btn btn-info float-right">
                            <i class="fa fa-plus"></i> Admin
                        </a>
                </div><br><br>
                <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                </div>
            </div>
            <div class="box-body">
                <div class="col-md-12">
                    <table  id="table1" class="display table table-bordered table-striped table-hover" border="1">
                        <thead> 
                            <tr>
                                <th>Sr.No</th>
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                            @if(count($admins) == 0)
                            <tr class="odd">
                                <td colspan='8'>No search results found<td>
                            </tr>
                            @else
                            
                           <?php $i = ($admins->currentPage() == 1) ? 1 : $admins->currentPage() + 24; ?>
                            @foreach($admins as $key => $value)
                            <tr>
                                <td>{{ $i++}}</td>
                                <td>{{ $value->full_name}}</td>
                                <td>{{ $value->email}}</td>
                                <td>{{ $value->mobile_no}}</td>
                                <td>{{ ($value->is_active==1)?'Active':'Deactive'}}</td>
                                <td>{{ $value->created_at}}</td>
                                <td>
                                        <a href="{{ route('admin.edit',$value->id) }}" title="Edit" type="button" data-toggle="tooltip" ><i class="fa fa-edit"  style="font-size:16px"></i></a>                     
                                        @if($value->is_active == '1')
                                        <a href="{{ url('admin/activate/'.$value->id,0) }}" title="Deactivate" data-toggle="tooltip"  onclick="return confirm('Are you sure you want to deactivate this?');" type="button"><i class="fa fa-times" style="font-size:16px" ></i></a>
                                        @else 
                                        <a href="{{ url('admin/activate/'.$value->id,1) }}" title="Activate" data-toggle="tooltip"  onclick="return confirm('Are you sure you want to Activate this?');" type="button"><i class="fa fa-check" style="font-size:16px"></i></a>
                                        @endif
                                </td> 
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>    
                </div>
                <nav class="text-right">
                    @if ($admins->lastPage() > 1)
                    <ul class="pagination">
                        <li class="page-item {{ ($admins->currentPage() == 1) ? ' disabled' : '' }}">
                            <a class="page-link" href="{{ $admins->url($admins->currentPage() - 1) }}">Prev</a>
                        </li>
                        @for ($i = 1; $i <= $admins->lastPage(); $i++)
                        <li class="page-item {{ ($admins->currentPage() == $i) ? ' active' : '' }}">
                            <a class="page-link" href="{{ $admins->url($i) }}">{{ $i }}</a>
                        </li>
                        @endfor
                        <li class="page-item {{ ($admins->currentPage() == $admins->lastPage()) ? ' disabled' : '' }}">
                            <a class="page-link" href="{{ $admins->url($admins->currentPage()+1) }}" >Next</a>
                        </li>
                    </ul>
                    @endif
                </nav>

            </div>

        </div>
    </section>
</div>

@endsection
