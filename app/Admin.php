<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Admin extends Authenticatable
{
     protected $fillable = ['email','middle_name','full_name','first_name','last_name', 'mobile_no', 'password','gender', 'is_active','is_delete'];
    //not using the default table name as per laravel convention which will be products
    protected $table = 'admin';
    //mark columns as dates - this gives us carbon instances of these dates automatically
    protected $dates = ['created_at', 'updated_at'];
}

