<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use DB;
use Config;
use Session;
use Auth;
use Storage;
use Hash;
class AdminsController extends Controller
{
   /* public function __construct() {
        $this->middleware('auth:admin', ['except' => array('login')]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       /* $data = array();
        $session_data = session()->get('filterSession');
        if (!empty($session_data['search_text'])) {
            $data[] = ['admin.full_name', 'LIKE', '%' . $session_data['search_text'] . '%'];
            // dd($data);
        }*/
        $admins = DB::table('admin')
        //->where($data)
        ->select('admin.*')
        ->paginate(25);
        
      //  flash()->success('Admin Created Successfully');
        return view('admin.index',compact('admins'));
    }

    
    public function create()
    {
        return view('admin.create', compact(''));
    }

    public function store(Request $request)
    {
        $exist = Admin::where('email', '=', $request->email)->first();
        if (empty($exist)) {
            $request->request->add(['created_by' => 1, 'updated_by' => 1, 'password' => bcrypt($request->password), 'first_name' => ucwords($request->first_name), 'last_name' => ucwords($request->last_name),'is_active' => '1', 'role' => 1]);
            $admin = new Admin();
            $admin->full_name = ucwords($request->first_name) . ' ' . ucwords($request->last_name);
            $admin->is_delete = 1;
           /* $user = Auth::guard('admin')->user();
            if (!empty($user)) {
                $admin->created_by = $user->id;
                $admin->modified_by = $user->id;
            } else {
                $admin->created_by = 1;
                $admin->modified_by = 1;
            }*/
            $admin->fill($request->all()); //sets the right values
            $admin->save();
            $request->session()->flash('success', 'Admin Created Successfully');
            return redirect()->route('admin.index');
        } else {
            $request->session()->flash('errors', 'This Email Already Exists');
            return redirect()->route('admin.create');
        }
        return redirect()->route('admin.create');
    }

        public function edit($id)
    {
        $data = Admin::find($id);
        $status = Config::get('aarogyam.status');
        return view('admin.edit', compact('data','status'));
    }

    
    public function update(Request $request, $id)
    {
        $admin = Admin::find($id);
        $admin->fill($request->all());
        $admin->first_name = ucwords($request->first_name);
        $admin->last_name = ucwords($request->last_name);
        $admin->full_name = ucwords($request->first_name) . ' ' . ucwords($request->last_name);
        $admin->updated_at = date('Y-m-d H:i:s');
        if ($admin->save()) {
            $request->session()->flash('success', 'Admin Has Been Updated!');
        } else {
            $request->session()->flash('errors', 'Unable To Update Admin!');
        }
        return redirect()->route('admin.index');
    }
    /*
    public function activate($id, $status) {
        $admin = Admin::find($id);
        $admin->is_active = $status;
        if($status==0){
            \Session::flash('flash_message', 'Admin Deactivated Successfully!');
        }
        else{
            \Session::flash('flash_message', 'Admin Activated Successfully!');
        }
        
        $admin->save();
        return redirect()->route('admin.index');
    }
    public function login() {
        return view('admin_login');
    }

    
    
    public function ChangePassword(Request $request)
    {
        return view('admin.admin.changePassword');
    }
    public function updatePassword(Request $request)
    {
        // dd($request);
        $user = Auth::guard('admin')->user();
        $check=Hash::check($request->oldPassword, $user->password, []);
        if($check == 'true'){
         $change = Admin::find($user->id);
         $change->password = bcrypt($request->newPassword);
          if($change->save()){
            Session()->flash('success', 'Admin Has Been Updated!');
          }
          else{
            Session()->flash('errors', 'Unable To Update Admin!');
          }
        } else{
        Session()->flash('errors', 'Password do not match');
        }
    
    return redirect()->route('admin.index');
    }
    */
}
